import React from 'react';
import { View, StyleSheet } from 'react-native';

const ViewsScreen = () => {
  return (
    <View style={styles.mainView}>
      <View style={styles.viewOne} />
      <View style={styles.viewTwo} />
      <View style={styles.viewThree} />
    </View>
  );
};

const styles = StyleSheet.create({
  mainView: {
    height: 200,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  viewOne: {
    width: 100,
    height: 100,
    borderWidth: 2,
    borderColor: 'red',
    backgroundColor: 'rgba(255,0,0,0.5)',
  },
  viewTwo: {
    width: 100,
    height: 100,
    borderWidth: 2,
    borderColor: 'green',
    backgroundColor: 'rgba(0,255,0,0.5)',
    top: 100,
  },
  viewThree: {
    width: 100,
    height: 100,
    borderWidth: 2,
    borderColor: 'blue',
    backgroundColor: 'rgba(0,0,255,0.5)',
  },
});

export default ViewsScreen;
