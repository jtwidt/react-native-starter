// PART 1 - Import libraries
import React from 'react';
import { Text, StyleSheet, View } from 'react-native';

// PART 2 - Create component
const ComponentsScreen = () => {
  const name = 'Jeremy';

  return (
    <View>
      <Text style={styles.started}>Getting started with React Native!</Text>
      <Text style={styles.greeting}>My name is {name}</Text>
    </View>
  );
};

// PART 3 - Create a stylesheet for the component
const styles = StyleSheet.create({
  started: {
    fontSize: 45,
  },
  greeting: {
    fontSize: 20,
  },
});

// PART 4 - Export the component
export default ComponentsScreen;
